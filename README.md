**Algorithms 1**
============
The implementation of basic abstract data types.

##### The project is divided into three parts: #####
 * c201.c: Linked list
 * c202.c: Stack of characters in the array
 * c206.c: Doubly linked list